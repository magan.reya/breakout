package breakout;

import javafx.scene.text.Text;

public class TextNode {
    private javafx.scene.text.Text TextBox;

    public Text setText(int x, int y){
        /*
        This method initializes the text in the game scene
         */
        TextBox = new javafx.scene.text.Text();
        TextBox.setX(x);
        TextBox.setY(y);
        return TextBox;
    }
}
