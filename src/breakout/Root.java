package breakout;

import javafx.scene.Group;
import javafx.scene.Node;

public class Root {
    public Group root = new Group();
    public void setRoot(Node sceneElement){
        root.getChildren().add(sceneElement);
    }

    public Group getRoot(){
        return root;
    }
}
