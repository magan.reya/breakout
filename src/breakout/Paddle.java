package breakout;

import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

public class Paddle {
    private Rectangle Paddle;

    public Rectangle setPaddle(int width, int height, Paint fill, int x, int y){
        /*
        This method initializes the paddle in the game scene
         */
        Paddle = new Rectangle(width, height, fill);
        Paddle.setX(x);
        Paddle.setY(y);
        return Paddle;
    }
}
