package breakout;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.util.ArrayList;

public class Main extends Application {
    /*
    Main class that starts the game and keeps track of game play
     */
    //Game Screen Parameters
    private static final String TITLE = "Breakout";
    private static final int SIZE = 400;
    private static final int FRAMES_PER_SECOND = 40;
    private static final double SECOND_DELAY = 1.0 / FRAMES_PER_SECOND;
    private static final Paint BACKGROUND = Color.LIGHTBLUE;

    //Paddle Parameters
    private static final int PADDLE_SPEED = 5;
    private static final Paint PADDLE_COLOR = Color.MAROON;
    private static final int PADDLE_INITIAL_X = 180;
    private static final int PADDLE_INITIAL_Y = 320;
    private static final int PADDLE_WIDTH = 40;
    private static final int PADDLE_HEIGHT = 5;

    //Ball Parameters
    public static final int BALL_SIZE = 7;
    public static final Paint BALL_COLOR = Color.BLACK;
    public static int BALL_X_VELOCITY = 3;
    public static int BALL_Y_VELOCITY = 3;
    private int BALL_INITIAL_X = SIZE/2 - BALL_SIZE;
    private int BALL_INITIAL_Y = PADDLE_INITIAL_Y;

    //Score and Live Display Parameters
    private static final int SCORE_AND_LIVES_X = 100;
    private static final int SCORE_AND_LIVES_Y = 350;

    //Ending Text Display Parameters
    private static final int GAME_OVER_X = 150;
    private static final int GAME_OVER_Y = 250;

    //Bricks Parameters
    private static final int BRICKS_WIDTH = 40;
    private static final int BRICKS_HEIGHT = 20;
    private static final Paint BRICKS_OUTLINE = Color.BLACK;

    //Relevant Global Variables
    private Scene myScene;
    private Rectangle myPaddle;
    private Circle myBall;
    public Group root;
    private Text myScoreandLives;
    private Text gameOver;
    private ArrayList<Rectangle> bricks;
    private int count = 0;
    private int lives = 3;
    private int missedThreshold = 350;
    private int maxScore = 70;
    private int center = 200;

    Collisions collisionObject = new Collisions(); //Class to deal with collisions
    Ball ball = new Ball();
    Root rootCall = new Root();
    Paddle paddle = new Paddle();
    TextNode text = new TextNode();
    Bricks brick = new Bricks();




    @Override
    public void start (Stage stage) {
    /*
    This method is borrowed from Dr.Robert Duvall's set up in the class exercise Example_Animation. It sets up the initial scene
    and animation and then deals with the game play. (https://coursework.cs.duke.edu/compsci307_2021fall/example_animation/-/blob/master/src/animation/ExampleAnimation.java)
     */

        myScene = setupGame(SIZE, SIZE, BACKGROUND); //set up scene with dimensions and objects
        stage.setScene(myScene);
        stage.setTitle(TITLE);
        stage.show();

        Timeline animation = new Timeline(); //keep animation running
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.getKeyFrames().add(new KeyFrame(Duration.seconds(SECOND_DELAY), e -> gamePlay()));
        animation.play();
    }



    private Scene setupGame (int width, int height, Paint background) {
        /*
    This method sets up the relevant nodes (paddle, ball, text nodes) that are needed in the game. It communicates with
    the Initialize_Element class which goes through each element and sets it up and adds to the Group. This method returns
    the final scene with all the elements displayed. The scene has width x height dimensions and is filled with 'background' color.
     */
        myPaddle = paddle.setPaddle(PADDLE_WIDTH, PADDLE_HEIGHT, PADDLE_COLOR, PADDLE_INITIAL_X, PADDLE_INITIAL_Y);
        rootCall.setRoot(myPaddle);
        myBall = ball.setBall(BALL_SIZE, BALL_INITIAL_X, BALL_INITIAL_Y, BALL_COLOR);
        rootCall.setRoot(myBall);
        myScoreandLives = text.setText(SCORE_AND_LIVES_X, SCORE_AND_LIVES_Y);
        rootCall.setRoot(myScoreandLives);
        gameOver = text.setText(GAME_OVER_X, GAME_OVER_Y);
        rootCall.setRoot(gameOver);
        bricks = brick.setBricks(BRICKS_WIDTH, BRICKS_HEIGHT, BRICKS_OUTLINE);
        for(int k = 0; k < bricks.size(); k ++){
            rootCall.setRoot(bricks.get(k));
        }
        root = rootCall.getRoot();
        Scene scene = new Scene(root, width, height, background);
        scene.setOnKeyPressed(e -> paddleMovement(e.getCode()));
        System.out.print(root);

        return scene;
    }

    private void gamePlay () {
        /*
    This method deals with all the collisions that can happen in the game. It communicates with the Collisions class which
    changes the x and y velocity of the ball dependent on where it collides.
     */
        ball.setPosition(myBall, myBall.getCenterX(), BALL_X_VELOCITY, myBall.getCenterY(), BALL_Y_VELOCITY);

        BALL_X_VELOCITY = collisionObject.ballXCollisions(myBall, myScene, BALL_X_VELOCITY);
        BALL_Y_VELOCITY = collisionObject.ballYCollisions(root, myBall, myPaddle, BALL_Y_VELOCITY, BALL_X_VELOCITY, bricks);
        textDisplay();
    }
    private void textDisplay(){
        /*
    This method displays the current score and number of lives left in the game. It also displays the ending screens.
     */
        count = collisionObject.getCount();
        myScoreandLives.setText("Current score is:" + " " + count + "   Remaining Lives:" +  lives);
        if(myBall.getCenterY() > missedThreshold){ //if the ball goes below the xcoordinate of 350, it is considered missed, so a life is lost
            myBall.setCenterX(BALL_INITIAL_X);
            myBall.setCenterY(BALL_INITIAL_Y - 10);
            myPaddle.setX(PADDLE_INITIAL_X);
            myPaddle.setY(PADDLE_INITIAL_Y);
            lives --;
        }
        if(lives <= 0){ //if all lives are lost, player loses the game
            root.getChildren().remove(myBall);
            myBall.setCenterX(center);
            myBall.setCenterY(center);
            gameOver.setText("Game Over! You Lose :(");

        }
        if(count >= maxScore){ //if all the bricks are broken, player wins the game
            root.getChildren().remove(myBall);
            myBall.setCenterX(center);
            myBall.setCenterY(center);
            gameOver.setText("Game Over! You Win :)");
        }
    }

    private void paddleMovement (KeyCode code) {
        /*
    Some of this method is borrowed from Dr. Robert Duvall's set up in the class exercise Example_Animation. It sets up the Right and Left keys to
    control paddle movement. (https://coursework.cs.duke.edu/compsci307_2021fall/example_animation/-/blob/master/src/animation/ExampleAnimation.java)
     */
        if (code == KeyCode.RIGHT){
            myPaddle.setX(myPaddle.getX() + PADDLE_SPEED);
        }
        else if (code == KeyCode.LEFT){
            myPaddle.setX(myPaddle.getX() - PADDLE_SPEED);
        }

    }

}
