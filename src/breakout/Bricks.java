package breakout;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

public class Bricks {
    ArrayList<Rectangle> Bricks = new ArrayList<Rectangle>();
    public Paint BRICK_COLOR;

    public ArrayList<Rectangle> setBricks(int width, int height, Paint outline) {
        /*
        This method creates all the bricks in the game scene
         */
        int brick_y_value = 0;
        int brick_x_value = 0;
        for (int k = 1; k <= 10; k++) {
            if (brick_y_value % 3 == 0) {
                BRICK_COLOR = Color.PINK;
            } else {
                BRICK_COLOR = Color.RED;
            }
            Rectangle myBrick = new Rectangle(width, height, BRICK_COLOR);
            myBrick.setX(brick_x_value);
            myBrick.setY(brick_y_value);
            myBrick.setStroke(outline);
            brick_x_value += 40; //each brick is 40 x 20 so after one brick is made it goes 40 to the right to create the next brick
            Bricks.add(myBrick); //an arraylist that contains every brick
            if (k == 10 && brick_y_value <= 100) { //want to go to a y value of 120, so this above loop will restart until then
                brick_y_value += 20; //brick dimensions are 40 x 20 so after 10 bricks the next line is made 20 coordinates down
                brick_x_value = 0; //the x coordinate of the starting brick of a new line is 0
                k = 0; //restart for loop
            }
        }

        return Bricks;
    }
}
