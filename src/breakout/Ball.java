package breakout;

import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class Ball {

    private Circle Ball;

    public Circle setBall(int size, int x, int y, Paint fill){
        /*
        This method initializes the ball in the game scene
         */
        Ball = new Circle(x, y, size, fill);
        return Ball;
    }

    public void setPosition(Circle myBall, double x, int xvelocity, double y, int yvelocity){
        myBall.setCenterX(myBall.getCenterX() + (xvelocity));
        myBall.setCenterY(myBall.getCenterY() - (yvelocity));
    }
}
