package breakout;

import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.Group;
import java.util.ArrayList;

public class Collisions {
    /*
    this class deals with the collisions within the game
     */
    public int count = 0;

    public int ballXCollisions(Circle ball, Scene scene, int x_velocity){
        /*
        This method changes the x velocity direction for the ball dependent on collisions
         */
        if (ball.getCenterX() >= scene.getWidth()){
            x_velocity = -x_velocity;
        }
        if (ball.getCenterX() <= 0){
            x_velocity = -x_velocity;
        }
        return x_velocity;
    }
    public int ballYCollisions(Group root, Circle ball, Rectangle paddle, int y_velocity, int x_velocity, ArrayList<Rectangle> bricks){
        /*
        This method changes the y velocity direction for the ball dependent on collisions
         */
        if(ball.getCenterY() <= 0){
            y_velocity = -y_velocity;
        }
        for(int k = 0; k < bricks.size(); k ++){ //iterate through bricks arraylist to find any ball and brick collision
            if(isIntersecting(bricks.get(k), ball) && root.getChildren().contains(bricks.get(k))){ //if the ball hits a brick that has not already been hit
                root.getChildren().remove(bricks.get(k));
                count ++;
                y_velocity = -y_velocity;
            }
        }
        if (isIntersecting(paddle, ball)) {
            ball.setCenterX(ball.getCenterX() + (x_velocity));
            y_velocity = -y_velocity;
        }
        return y_velocity;
    }
    public int getCount(){
        /*
        this method returns the current score
         */
        return count;
    }
    private boolean isIntersecting (Rectangle a, Circle b) {
        /*
        This method is borrowed from Dr.Duvall's example in class (example_animation) and checks for intersections between the ball and a rectangular object (paddle, brick)
        (https://coursework.cs.duke.edu/compsci307_2021fall/example_animation/-/blob/master/src/animation/ExampleAnimation.java)
         */
        return b.getBoundsInParent().intersects(a.getBoundsInParent());

    }

}
