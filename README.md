game
====

This project implements the game of Breakout.

Name: Reya Magan

### Timeline

Start Date: Saturday 8/28

Finish Date: Monday 8/30

Hours Spent: 14

### Tutorial and other Resources Used
I used much of the learning from the class lab: Example_Animation. In this lab I learned about the functionality of JavaFX and how to use it to create an animation. Additionally, I borrowed code from the start, isIntersecting, and handleKeyInput methods.
The link is here: https://coursework.cs.duke.edu/compsci307_2021fall/example_animation/-/blob/master/src/animation/ExampleAnimation.java

I also found out about the Text element in JavaFX with this link: https://www.tutorialspoint.com/javafx/javafx_text.htm

I needed a quick refresher on how to call a new class in Java, so I used the example by UVM in this stackexchange thread: https://stackoverflow.com/questions/11360118/pass-an-object-created-in-one-class-to-another-class/11360468

Finally, I wanted to understand the Scene and Timeline elements with JavaFX better so I read up on these links: https://www.tutorialspoint.com/javafx/javafx_application.htm , https://docs.oracle.com/javase/8/javafx/api/javafx/animation/Timeline.html

### Resource Attributions
I only used the above websites, I do not have any images or other resources in this game.

### Running the Program

Main class: Main.java

Data files needed: N/A

Key/Mouse inputs: Right and Left Arrows to move paddle horizontally

Known Bugs: None to my knowledge. For a while I had a bug in which the ball would disappear behind the "broken" bricks and the player would not be able to see it, but I fixed it by sending it to the front of the screen. I had some bugs when I tried to use multiple classes, as well, but those were typical typos or gaps in my understanding - ultimately I was able to get it to work.


### Notes/Assumptions

I didn't really understand how to delete a brick after it has intersected with the ball, so I ended up just changing its color and sending it to the back of the screen. The ball would then just pretend like this brick was part of the background. This worked, but I don't know that it is the best way to tackle this issue. In the same way, once the game ends I just set the ball's color to the background color instead of deleting it.


### Impressions

I really enjoyed this project! I have never coded a game before, and this was the first time I'd created something largely from scratch. I felt like I learned a lot and had fun while doing so - I didn't even realize I'd been working for almost 6 hours straight because I enjoyed it so much. I would have loved to add more features to my game, I think it would be cool to have this be a bigger project where we continue to add more features to make it a really dynamic and engaging game. I'll probably try making my game have more features during some free time!

